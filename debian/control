Source: pyatspi
Section: python
Priority: optional
Maintainer: Debian Accessibility Team <debian-accessibility@lists.debian.org>
Uploaders: Samuel Thibault <sthibault@debian.org>
Vcs-Browser: http://git.debian.org/?p=pkg-a11y/pyatspi.git;a=summary
Vcs-Git: git://git.debian.org/git/pkg-a11y/pyatspi.git
Homepage: http://live.gnome.org/Accessibility
Build-Depends: debhelper (>= 7.0.50~),
               python-all, python3-all,
               pkg-config,
               python-gi-dev (>= 2.90.1),
               dh-autoreconf
X-Python-Version: >= 2.6
Standards-Version: 3.9.6

Package: python-pyatspi
Architecture: all
Breaks: python-pyatspi2 (<< 2.7.2+dfsg-2)
Replaces: python-pyatspi2 (<< 2.7.2+dfsg-2)
Depends: ${misc:Depends},
         ${python:Depends},
         python-gi (>= 2.90.1),
         gir1.2-atspi-2.0 (>= 2.9.90),
         libatk-adaptor (>= 2.11.2), libgail-common, at-spi2-core, qt-at-spi
Description: Assistive Technology Service Provider Interface - Python bindings
 at-spi is the "Assistive Technology Service Provider Interface".
 .
 This package contains Python bindings for the client side aspects of at-spi2.

Package: python-pyatspi2
Section: oldlibs
Priority: extra
Architecture: all
Depends: ${misc:Depends},
         python-pyatspi (= ${source:Version})
Description: Transitional package for assistive technology Python bindings
 at-spi is the "Assistive Technology Service Provider Interface".
 .
 This empty transitional package can be safely removed.

Package: python3-pyatspi
Architecture: all
Breaks: python3-pyatspi2 (<< 2.7.2+dfsg-2)
Replaces: python3-pyatspi2 (<< 2.7.2+dfsg-2)
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-gi (>= 2.90.1),
         gir1.2-atspi-2.0 (>= 2.1.4),
         libatk-adaptor (>= 2.11.2), libgail-common, at-spi2-core, qt-at-spi
Description: Assistive Technology Service Provider Interface - Python3 bindings
 at-spi is the "Assistive Technology Service Provider Interface".
 .
 This package contains Python3 bindings for the client side aspects of at-spi2.

Package: python3-pyatspi2
Section: oldlibs
Priority: extra
Architecture: all
Depends: ${misc:Depends},
         python3-pyatspi (= ${source:Version})
Description: Transitional package for assistive technology Python3 bindings
 at-spi is the "Assistive Technology Service Provider Interface".
 .
 This empty transitional package can be safely removed.
